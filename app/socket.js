var systemd = require('./systemd');
var path = require('./config').path;
var database = require('./config').database;
var mysql = require('mysql');
var connectdomain = require('./user');

const objectAssign = require('object-assign');
module.exports = function(exec, myEmitter){
	
	myEmitter.on('adddomain', function(data){
		connectdomain.update({'email': data.email}, {$push:{'Domain':{
			'domain_name': data.domain_name,
			'create' : Date()
		}}}, function(error, resul){
			if(error){
				console.log(error);
			}else{
				console.log(resul);
			}
		});
	});

	myEmitter.on('deletedomain', function(data){
		connectdomain.update({'email': data.email}, {$pull:{'Domain':{
			'domain_name': data.domain_name
		}}}, function(error, resul){
			if(error){
				console.log(error);
			}else{
				console.log(resul);
			}
		});
	});

	myEmitter.on('deletel', function(data){
		let dbname = data.replace(/\./gi, '_') + "_db";
		let querydeletedb = `drop database ${dbname}`;
		let querydeleteuser = `drop user '${data}'@'localhost'`;
		let querydelete = `${querydeletedb};${querydeleteuser};`;
		objectAssign(database, {'database': ''});
		let connect = mysql.createPool(database);
		exec(`rm -rf ${path}/${data}`);
		connect.getConnection(function(error, connection){
			connection.query(querydelete, [3,2,1]);
		});
	});

	myEmitter.on('show', function(data){
		exec(`rm -rf ${path}/${data.domain}/public/.git`);
	});
}