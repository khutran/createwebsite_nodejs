var connect = require('./user');
const objectAssign = require('object-assign');
var jwt = require('jsonwebtoken');
var Q = require('q');

var authentokens = function(tokens){
	// console.log(tokens);
	return Q.promise((response) => {
		jwt.verify(tokens, '!43K2mImO31#wth',  function(err, decode){
		 	if(err){
		 		return response({'stt': 3, 'notification': err.name});
		 	}else if(!decode.exp){
		 		return response({'stt': 3, 'notification': 'tokens illegal'});
		 	}else{
		 		return response({"stt": 0, 'decode': decode.data});
			}
		});
	});
}

module.exports = {

	Add : function(tokens, callback){
		authentokens(tokens)
		.then(function(decode){
	    	if(decode.stt === 0){
				connect.findOne({'email': decode.decode.email}, function(err, email){
					if(err){
						return callback({'stt': 5, 'notification': err});
					}
					else if(email){
						return callback({'stt': 2, 'notification': 'email exits'});
					}else{
						var newuser = {"email": decode.decode.email, "date_create": Date()};
						var new_user = new connect(newuser);			
						new_user.save(function(error){
							if(error){
									return callback({'stt': 5, 'notification': error});
								}else{
									return callback({'stt': 0, 'notification': newuser});
								}
							});	
					}
				});
	    	}else{
	    		return callback(decode);
	    	}
	    });
	},
	authreq : function(req, res, next){
		authentokens(req.headers.authorization)
			.then(function(decode){
				if(decode.stt === 0){
					connect.findOne({'email': decode.decode.email}, function(error, results){
						if(error){
							res.json({'stt': 5, 'notification': error});
							res.end();
						}else if(!results){
							res.json({'stt': 3, 'notification': 'email notfound'});
							res.end();
						}else{
							req.email = decode.decode.email;
					 		return next();
						}
					});
				}else{
					res.statusCode = 500;
					res.json(decode);
				}
			});	
	},

	edituser: function(tokens, callback){
		authentokens(tokens)
		.then(function(decode){
			if(decode.stt == '0'){
				// console.log(decode.decode.contentnew);
				connect.findOneAndUpdate({"email":decode.decode.email}, {$set: decode.decode.contentnew}, function(error, results){
					if(error){
						return callback({'stt': '5', 'notification': error});
					}else if(!results){
						return callback({'stt': '3', 'notification': 'email notfound'});
					}else{
						return callback({'stt': '0', 'notification': 'update sucess'});
					}
				});
			}else{
				res.json(decode);
				res.end();
			}
		});
	},

	deleteuser: function(tokens, callback){
		authentokens(tokens)
		.then(function(decode){
			connect.remove({"email": decode.decode.email}, function(error, data){
				if(error){
					return callback({'stt': '5', 'notification': error});
				}else{
					return callback({'stt': '0', 'notification':data});
				}
			});
		});
	},

	show_domain: function(data, callback){
		connect.findOne({'email': data}, function(error, results){
			if(error){
				return callback({'stt': 5, 'email': data, 'Domain': error});
			}else{
				return callback({'stt': 0, 'email': data, 'Domain': results.Domain});
			}
		});
	}
}
return module.exports;
