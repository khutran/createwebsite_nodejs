var database = require('./config').database;
var path = require('./config').path;
var mysql = require('mysql');
var md5 = require('md5');
var moment = require('moment');
var exec = require('child_process').exec;
const objectAssign = require('object-assign');
var Q = require('q');

module.exports = {
	importdb: function(config, callback){
		var create = function(configs){
				objectAssign(database, {'database': ''});
				let connect = mysql.createPool(database);
				let dbname = config.domain.replace(/\./gi, '_') + "_db";
				let querydb = `create database ${dbname}`;
				let queryuser = `create user '${config.user}'@'localhost' identified by '${config.password}'`;
				let querydbtouser = `grant all privileges on \`${dbname}\`.* to '${config.user}'@'localhost'`;
				let queryrun = `${querydb};${queryuser};${querydbtouser};`;
				return Q.promise((response) =>{
					connect.getConnection((error, connection) => {
						connection.query(`${queryrun}`, [3,2,1] , function(err, results, fields){
							if(err){
								return response({'stt': '5', 'notification': err});
							}else{
								return response({'user': config.user,'database': dbname, 'password': config.password});
							}
						});
					});
				});
		}

		var replace = function(configs, search){
			return Q.promise((response)=>{
				exec(`php replace/srdb.cli.php -n ${configs.database} -u ${configs.user} -p ${configs.password} -h localhost -s ${search} -r http://${configs.user}`, function(err, results, fields){
					if(err){
						return response({'stt': '5', 'notification': err});
					}else if(results.indexOf('SQLSTATE') > -1){
						return response({'stt': '5', 'notification': results});
					}else{
						return response({'notification': 'suscess'});
					}
				});
			});
		}

		var createadmin = function(connect){
			let pass = Math.random().toString(36).slice(-8);
			let passmanager = md5(pass);
			let date = moment().format('YYYY-MM-DD h:mm:ss');
			let queryimportwpuser = `INSERT INTO \`wp_users\` (\`ID\`, \`user_pass\`, \`user_email\`,  \`user_nicename\`, \`display_name\`, \`user_registered\`, \`user_login\`) VALUES ('999', '${passmanager}' ,'manager@gmail.com', 'manager', 'manager', '${date}' ,'manager')`;
			let queryimportmetadata = `INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'nickname', 'manager');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'first_name', 'manager');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'last_name', 'manager');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'description', '');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'rich_editing', 'true');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'comment_shortcuts', 'false');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'admin_color', 'fresh');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'use_ssl', '0');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'show_admin_bar_front', 'true');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'locale', '');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'wp_user_level', '10');INSERT INTO \`wp_usermeta\` (\`user_id\`, \`meta_key\`, \`meta_value\`) VALUES (999, 'dismissed_wp_pointers', '')`;		
			let findsite = `SELECT \`option_value\` FROM \`wp_options\` WHERE \`option_name\` = 'siteurl'`;
			let queryrun = `${findsite};${queryimportwpuser};${queryimportmetadata}`;
			return Q.promise((response)=>{
				connect.query(queryrun, [3,2,1], function(err, results, fields){
					if (err){
						return response({'stt': '5', 'notification': err});
					}else{
						return response({'admin':'manager', 'passadmin': pass, 'search':results[0][0].option_value});

					}
				});
			});
		}

		create(config)
		.then(function(dd){
			if(dd.stt){
				return callback(dd);
			}else{
				objectAssign(database, {'database': dd.database});
				exec(`mysql -u ${dd.user} -p${dd.password} ${dd.database} < ${path}/${dd.user}/public/database/wordpress.sql`,function(error, stdout, stderr){
					if(error){
						return callback({'stt': '5', 'notification': 'error import database'});
					}else{
						let newconnect = mysql.createPool(database);
						createadmin(newconnect)
						.then(function(results){
							replace(dd, results.search)
							.then(function(dirvice){
								if(dirvice.stt){
									return callback(dirvice);
								}else{
									return callback(results);
								}

							});
						});

					}
				});
			}
		});
	}
}
return module.exports;