var createdoamin = require('./systemd').createdoamin;
var createfileenv = require('./systemd').createfileenv;
var deletel = require('./systemd').deletel;
var importdb = require('./mysql').importdb;
var authreq = require('./mongodb').authreq;
const objectAssign = require('object-assign');
var exec = require('child_process').exec;
var events = require('events');
var adduser = require('./mongodb').Add;
var deleteuser = require('./mongodb').deleteuser;
var edituser = require('./mongodb').edituser;
var show_domain = require('./mongodb').show_domain;
var myEmitter = new events.EventEmitter();
require("./socket")(exec, myEmitter);

module.exports = function(app) {
    app.post('/create', authreq, (req, res) => {
        let domain = req.body.domain;
        let git = req.body.git;

        if (!domain || !git) {
            res.statusCode = 500;
            res.json({ notification: "domain or git empty" });
        } else {
            createdoamin(domain, git, function(results) {
                switch (results.stt) {
                    case 0:
                        createfileenv(domain)
                            .then(function(data) {
                                objectAssign(results, data);
                                if (results.stt === 0) {
                                    importdb(results, function(xxx) {
                                        objectAssign(results, xxx);
                                        if (results.stt === 0) {
                                            myEmitter.emit('show', results);
                                            myEmitter.emit('adddomain', { 'domain_name': results.domain, 'email': req.email });
                                            res.json({ 'notification': results.notification, 'domain': results.domain, 'user': results.admin, 'password': results.passadmin });

                                        } else {
                                            myEmitter.emit('delete', results);
                                            res.statusCode = 500;
                                            res.json({ 'stt': results.stt, 'notification': results.notification, 'domain': results.domain });

                                        }
                                    });
                                } else {
                                    myEmitter.emit('delete', results);
                                    res.statusCode = 500;
                                    res.json({ 'stt': results.stt, 'notification': results.notification, 'domain': results.domain });

                                }
                            });
                        break;
                    case 1:
                        res.statusCode = 500;
                        res.json(results);

                        break;
                    case 2:
                        myEmitter.emit('delete', results);
                        res.statusCode = 500;
                        res.json({ 'stt': results.stt, 'notification': results.notification, 'domain': results.domain });

                        break;
                    default:
                        res.statusCode = 500;
                        res.end();
                }
            });
        }
    });

    app.get('/show_domain', authreq, function(req, res) {
        if (!req.email) {
            res.statusCode = 500;
            res.json({ notification: "email empty" });
        } else {
            show_domain(req.email, function(data) {
                if (data.stt != 0) {
                    res.statusCode = 500;
                    res.json(data);
                } else {
                    res.json(data);
                }

            });
        }
    });

    app.delete('/delete', authreq, function(req, res) {

        var domain = req.body.domain;
        if (!domain) {
            res.statusCode = 500;
            res.json({ notification: "domain empty" });
        } else {
            deletel(domain, function(results) {
                myEmitter.emit('deletedomain', { 'email': req.email, 'domain_name': domain });
                if (results.stt != 0) {
                    res.statusCode = 500;
                    res.json(results);
                } else {
                    res.json(results);
                }
            });
        }
    });

    app.post('/adduser', function(req, res) {
        let tokens = req.headers.authorization;
        adduser(tokens, function(results) {
            if (results.stt != 0) {
                res.statusCode = 500;
                res.json(results);
            } else {
                res.json(results);
            }
        });
    });

    app.delete('/deleteuser', function(req, res) {
        deleteuser(req.headers.authorization, function(results) {
            if (results.stt != 0) {
                res.statusCode = 500;
                res.json(results);
            } else {
                res.json(results);
            }
        });
    });

    app.put('/edituser', (req, res) => {
        let tokens = req.headers.authorization;
        edituser(tokens, function(results) {
            if (results.stt != 0) {
                res.statusCode = 500;
                res.json(results);
            } else {
                res.json(results);
            }
        });
    });

}