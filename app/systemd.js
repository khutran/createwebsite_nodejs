var exec = require('child_process').exec;
var fs = require('fs');
var Q = require('q');
var path = require('./config').path;
var mysql = require('mysql');
var database = require('./config').database;
const objectAssign = require('object-assign');
module.exports = {
	createdoamin: function(domain, git, callback){

		var checkwebexits = function(call1){
			if (fs.existsSync(`${path}/${domain}/public`)) {
				return callback({'stt': 1, 'notification': 'website_exits', 'domain': domain});
				return;
			}else{
				return call1('next');
			}
		};

		var clonegit = function(){
			return Q.promise(function(out){
				exec(`git clone ${git} ${path}/${domain}/public`, function(error, stdout, stderr){
					if (error) {
						return callback({'stt': 5, 'notification': stderr, 'domain': domain});
						return;
					}else{
						return out('success');
					}
				});
			});	
		};

		checkwebexits(function(next){
			if(next == 'next'){
				clonegit()
				.then(function(results){
					if(results){
						return callback({'stt': 0, 'notification': results, 'domain': domain});
					}
				});
			}
		});
	},
	createfileenv: function(domain){
		let dbname = domain.replace(/\./gi, '_') + "_db";
		let password = Math.random().toString(36).slice(-8);
		let content = `<?php\n \$_ENV = [\n "WP_HOME" => "http://${domain}",\n "WP_SITEURL" => "http://${domain}",\n "DB_NAME" => "${dbname}" ,\n "DB_USER" => "${domain}" ,\n "DB_PASSWORD" => "${password}" ,\n "DB_HOST" => "localhost"\n];\n?>`;
		return Q.promise((response) => {
			fs.writeFile(`${path}/${domain}/public/.env.php`, content ,function(error){
				if(error) {
					return response({'stt': 5, 'notification': error,'user': domain, 'password': password});
				}else{
					return response({'user': domain, 'password': password});
				}
			});			
		});
	},
	deletel : function(data, callback){
		let dbname = data.replace(/\./gi, '_') + "_db";
		let querydeletedb = `drop database ${dbname}`;
		let querydeleteuser = `drop user '${data}'@'localhost'`;
		let querydelete = `${querydeletedb};${querydeleteuser};`;
		objectAssign(database, {'database': ''});
		let connect = mysql.createPool(database);
		exec(`rm -rf ${path}/${data}`);
		connect.getConnection(function(error, connection){
			connection.query(querydelete, [3,2,1],function(err, results, fields){
				if(err){
					return callback({'stt': 5, 'notification': err});
				}else{
					return callback({'stt': 0, 'notification': `delete ${data} success`});
				}
			});
		});
	}
}

return module.exports;