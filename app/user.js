var url = require('./config').mongodb;
var mongoose = require('mongoose');

var Adduser = function(){
	mongoose.Promise = global.Promise;
	var Schema = mongoose.Schema;
	var User = new Schema({
		email 	     : { type: String, required: true, unique: true},
		date_create  : { type: Date},
		Domain : [
			{
				domain_name : {type: String, required: true, unique: true},
				create : { type: Date}
			}
		]
	});
	mongoose.connect(url, {useMongoClient: true});
	return mongoose.model('users', User);	
}

module.exports = Adduser();
