var express = require('express');
var app = express();
var server = require('http').Server(app);
var cors = require('cors');
// var io = require('socket.io')(server);

var bodyParser   = require('body-parser');
// var flash        = require('connect-flash');
// var cookieParser = require('cookie-parser');
// var session      = require('express-session');

// app.use(flash());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(function (req, res, next) {

        // Website you wish to allow to connects
        res.setHeader('Access-Control-Allow-Origin', '*');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);

        // Pass to next layer of middleware
        next();
    });
// app.use(session({
//     secret: 'ilovescotchscotchyscotchscotch',
//     cookie: {maxAge: 300000},
//     proxy: true,
//     resave: true,
//     saveUninitialized: true
// }));

require('./app/router')(app);

app.listen(3000);